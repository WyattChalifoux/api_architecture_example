# API Architecture Example

This simple project demonstrates the basic concept of an API without referring to 
traditional REST API concepts like endpoints, HTTP requests, etc. The purpose of this
is to highlight the underlying fundamentals of APIs, what they are, how to design them, etc.