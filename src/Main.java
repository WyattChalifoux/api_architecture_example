import ClawMachine.ClawMachine;

public class Main {

    //The main method is the CLIENT for the ClawMachine API
    public static void main(String[] args) {

        //Client using Claw Machine API
        ClawMachine clawMachine = new ClawMachine();

        //Client using Claw Machine API
        clawMachine.moveClawRight();

        //Client using Claw Machine API
        clawMachine.moveClawBackward();

        //Client using Claw Machine API
        clawMachine.moveClawRight();

        //Client using Claw Machine API
        String prize = clawMachine.dropClaw();

        System.out.println("I got: " + prize);
    }
}